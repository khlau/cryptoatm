//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract CryptoAtm {
    mapping(address => uint256) private userEthBalance;
	mapping(address => uint256) private userLinkBalance;

	// Network: Rinkeby
	// https://docs.chain.link/docs/link-token-contracts/
	address constant LINK_ADDRESS = 0x01BE23585060835E02B77ef475b0Cc51aA1e0709;

	AggregatorV3Interface internal ethUsdPriceFeed;
	AggregatorV3Interface internal linkUsdPriceFeed;

	/**
     * Network: Rinkeby
	 * https://docs.chain.link/docs/ethereum-addresses/
     */
    constructor() {
        ethUsdPriceFeed = AggregatorV3Interface(0x8A753747A1Fa494EC906cE90E9f37563A8AF630e);
		linkUsdPriceFeed = AggregatorV3Interface(0xd8bD0a1cB028a31AA859A21A3758685a95dE4623);
    }

	function getUserEthBalance(address _userAddr) public view returns (uint256)
	{
		return userEthBalance[_userAddr];
	}

	function getUserLinkBalance(address _userAddr) public view returns (uint256)
	{
		return userLinkBalance[_userAddr];
	}

	function getEthPriceInUsd() public view returns (int)
	{
		(
            uint80 roundID, 
            int price,
            uint startedAt,
            uint timeStamp,
            uint80 answeredInRound
        ) = ethUsdPriceFeed.latestRoundData();
        return price;
	}

	function getLinkPriceInUsd() public view returns (int)
	{
		(
            uint80 roundID, 
            int price,
            uint startedAt,
            uint timeStamp,
            uint80 answeredInRound
        ) = linkUsdPriceFeed.latestRoundData();
        return price;
	}

	function depositLink(uint256 _amount) public
	{
		IERC20 linkContract = IERC20(LINK_ADDRESS);

		require(linkContract.balanceOf(msg.sender) >= _amount, "Not enough LINK balance");

		linkContract.transferFrom(msg.sender, address(this), _amount);
		userLinkBalance[msg.sender] += _amount;
	}

	function depositEth() public payable
	{
		userEthBalance[msg.sender] += msg.value;
	}

	function withdrawEth(uint256 _amount) public 
	{
		require(userEthBalance[msg.sender] >= _amount, "Not enough ETH balance");
		userEthBalance[msg.sender] -= _amount;
		payable(msg.sender).transfer(_amount);
	}

	function withdrawLink(uint256 _amount) public 
	{
		IERC20 linkContract = IERC20(LINK_ADDRESS);

		require(userLinkBalance[msg.sender] >= _amount, "Not enough LINK balance");
		userLinkBalance[msg.sender] -= _amount;
		linkContract.transfer(msg.sender, _amount);
	}
}
