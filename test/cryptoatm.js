const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("CryptoAtm", function () {
	let cryptoAtm;
	let owner, alice, bob;
	
	beforeEach(async () => {
		const CryptoAtm = await ethers.getContractFactory("CryptoAtm");
		cryptoAtm = await CryptoAtm.deploy();
		await cryptoAtm.deployed();
		[owner, alice, bob] = await ethers.getSigners();
	});

	it("ETH deposit, withdrawal and balance are correct", async function () {
		console.log("Owner address:");
		console.log(owner.address);

		// Deposit
		expect(await cryptoAtm.getUserEthBalance(alice.address)).to.equal(0);
		await cryptoAtm.connect(alice).depositEth({ value: 2500 });
		expect(await cryptoAtm.getUserEthBalance(alice.address)).to.equal(2500);

		// Withdraw
		await cryptoAtm.connect(alice).withdrawEth(2500);
		expect(await cryptoAtm.getUserEthBalance(alice.address)).to.equal(0);
	});


});
